<?php
class Event extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('event_model');
		$this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->helper('file');
	}

	public function create(){
		if(isset($this->session->userdata['logged_in'])){
			if($this->session->userdata['logged_in']['urednik']){
				$this->load->helper('form');
				$this->load->library('form_validation');

				$this->form_validation->set_rules('title', 'Title', 'required');
				$this->form_validation->set_rules('text', 'Text', 'required');
				$this->form_validation->set_rules('kraj', 'Kraj', 'required');
				$this->form_validation->set_rules('datum', 'Datum', 'required');
				if($this->form_validation->run() === FALSE){
					$this->load->view('templates/header_urednik');
					$this->load->view('event/create');
					$this->load->view('templates/footer');
				}else{
					$this->event_model->set_event();
					$this->load->view('templates/header_urednik');
					$this->load->view('event/success');
					$this->load->view('templates/footer');
					
				}
			}
			else{
				$this->load->helper('form');
	
				// Load form validation library
				$this->load->library('form_validation');
	
				$data['message_display'] = 'Nisi Urednik';
				$this->load->view('templates/header');
				$this->load->view('user_authentication/login_form', $data);
				$this->load->view('templates/footer');
	
			}	
		}
		else{
			$this->load->helper('form');

			// Load form validation library
			$this->load->library('form_validation');

			$data['message_display'] = 'Prijavi se da lahko dodaš dogodek, če si urednik';
			$this->load->view('templates/header');
			$this->load->view('user_authentication/login_form', $data);
			$this->load->view('templates/footer');

		}
	}

	public function view($slug){

		if(isset($this->session->userdata['logged_in'])){
			$data['news_item'] = $this->event_model->get_event_where($slug);
			$data['title'] = $data['news_item']['Naslov'];
			$this->load->view('templates/header', $data);
        	$this->load->view('event/view', $data);
        	$this->load->view('templates/footer', $data);
		}
		else{
			$this->load->helper('form');

			// Load form validation library
			$this->load->library('form_validation');

			$data['message_display'] = 'Prijavise če želiš videti dogodke.';
			$this->load->view('templates/header');
			$this->load->view('user_authentication/login_form', $data);
			$this->load->view('templates/footer');

		}
	}


    public function index(){
		if(isset($this->session->userdata['logged_in'])){
		$data['news'] = $this->event_model->get_event();
		$data['title'] = "Prihajajoči dogodki";

		$this->load->view('templates/header', $data);
        $this->load->view('event/index', $data);
        $this->load->view('templates/footer', $data);
		}
		else{
			$this->load->helper('form');

			// Load form validation library
			$this->load->library('form_validation');

			$data['message_display'] = 'Signin to view a event!';
			$this->load->view('templates/header');
			$this->load->view('user_authentication/login_form', $data);
			$this->load->view('templates/footer');

		}
	}
	
	public function date(){
		if(isset($this->session->userdata['logged_in'])){
		$data['news'] = $this->event_model->get_event_date();
		$data['title'] = "Prihajajoči dogodki";

		$this->load->view('templates/header', $data);
        $this->load->view('event/index', $data);
        $this->load->view('templates/footer', $data);
		}
		else{
			$this->load->helper('form');

			// Load form validation library
			$this->load->library('form_validation');

			$data['message_display'] = 'Signin to view a event!';
			$this->load->view('templates/header');
			$this->load->view('user_authentication/login_form', $data);
			$this->load->view('templates/footer');

		}
	}
	public function alf(){
		if(isset($this->session->userdata['logged_in'])){
		$data['news'] = $this->event_model->get_event_alf();
		$data['title'] = "Prihajajoči dogodki";

		$this->load->view('templates/header', $data);
        $this->load->view('event/index', $data);
        $this->load->view('templates/footer', $data);
		}
		else{
			$this->load->helper('form');

			// Load form validation library
			$this->load->library('form_validation');

			$data['message_display'] = 'Signin to view a event!';
			$this->load->view('templates/header');
			$this->load->view('user_authentication/login_form', $data);
			$this->load->view('templates/footer');

		}
	}
	public function all(){
		if(isset($this->session->userdata['logged_in'])){
		$data['news'] = $this->event_model->get_event_all();
		$data['title'] = "Vsi dogodki";

		$this->load->view('templates/header', $data);
        $this->load->view('event/index2', $data);
        $this->load->view('templates/footer', $data);
		}
		else{
			$this->load->helper('form');

			// Load form validation library
			$this->load->library('form_validation');

			$data['message_display'] = 'Signin to view a event!';
			$this->load->view('templates/header');
			$this->load->view('user_authentication/login_form', $data);
			$this->load->view('templates/footer');

		}
	}
	public function old(){
		if(isset($this->session->userdata['logged_in'])){
		$data['news'] = $this->event_model->get_event_old();
		$data['title'] = "Pretekli dogodki";

		$this->load->view('templates/header', $data);
        $this->load->view('event/index2', $data);
        $this->load->view('templates/footer', $data);
		}
		else{
			$this->load->helper('form');

			// Load form validation library
			$this->load->library('form_validation');

			$data['message_display'] = 'Signin to view a event!';
			$this->load->view('templates/header');
			$this->load->view('user_authentication/login_form', $data);
			$this->load->view('templates/footer');

		}
	}
}